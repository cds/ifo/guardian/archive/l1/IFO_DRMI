import os
import re
import time

##########

IFO = os.getenv('IFO')
SITE = os.getenv('SITE')
if not IFO:
    raise ImportError("could not find IFO environment variable.")
if not SITE:
    raise ImportError("could not find SITE environment variable.")

RTCDS = '/opt/rtcds/{site}/{ifo}'.format(site=SITE.lower(), ifo=IFO.lower())
if IFO == 'H1':
    DAQ = 'h1dc0'
elif IFO == 'L1':
    DAQ = 'l1daqdc0'
MASTER = os.path.join(RTCDS, 'target', DAQ, 'master')

##########
# FE tools

def get_active_model_ini():
    """Iterator over active front end model (name, inifile) tuples.

    """
    re_ss = re.compile('{rtcds}/chans/daq/{IFO}(.*).ini'.format(rtcds=RTCDS, IFO=IFO))
    with open(MASTER) as f:
        for ini in f:
            ini = ini.strip()
            match = re_ss.match(ini)
            if match:
                model = match.group(1).lower()
                yield (model, ini)


def get_active_model_dcuid():
    """Iterator over active (model, dcuid) tuples.

    """
    re_fec = re.compile('\[.*:FEC-(.*)_CPU_METER\]')

    for (model, ini) in get_active_model_ini():
        with open(ini) as f:
            for line in f:
                g = re_fec.search(line)
                if g:
                    dcuid = g.group(1)
                    yield (model, dcuid)


def ezca_get_dcuid(modname):
    """Return DCUID number for a given full model name.

    """
    sys = modname[2:5].upper()
    rest = modname[5:].upper()
    if rest:
        prefix = '%s-%s_' % (sys, rest)
    else:
        prefix = '%s-' % (sys)
    chan = '%sDCU_ID' % (prefix)
    return int(ezca[chan])

##########
# Model generators

def get_active_models():
    for (name, dcuid) in get_active_model_dcuid():
        yield CDSFrontEndModel(name, dcuid)


def get_all_models():
    for (name, dcuid) in get_active_model_dcuid():
        yield CDSFrontEndModel(name, dcuid)
    for (name, dcuid) in get_pseudo_frontends():
        yield CDSBeckhoffSDF(name, dcuid)

##########
# FE class interface

class CDSFrontEndModel(object):
    """Interface to CDS front-end FEC channel interface.

    """
    def __init__(self, name, dcuid=None):
        """Initalize with the front end model `name`.

        Name should be without leading <ifo> prefix:

        e.g. 'lsc', 'susetmx', etc.

        If `dcuid` is not specified, it will be determined via an ezca
        read of the DCU_ID channel.

        """
        # FIXME: should be way to get name from the DCUID
        self.__ifo = IFO.lower()
        self.__name = name.lower()
        self.__DCUID = None
        if dcuid:
            self.__DCUID = int(dcuid)

    @property
    def name(self):
        """Model abbreviated name."""
        return self.__name

    @property
    def fullname(self):
        """Model name."""
        return '%s%s' % (self.__ifo, self.__name)

    @property
    def DCUID(self):
        """Model DCUID."""
        if not self.__DCUID:
            self.__DCUID = int(ezca_get_dcuid(self.fullname))
        return self.__DCUID
    FEC = DCUID
    dcuid = DCUID
    fec = DCUID

    def __str__(self):
        return "<%s '%s' (dcuid: %d)>" % (self.__class__.__name__,
                                          self.name,
                                          self.dcuid,
                                          )

    def __fec_chan(self, chan):
        return 'FEC-%d_%s' % (self.dcuid, chan)

    def __getitem__(self, chan):
        """Get FEC channel value."""
        return ezca[self.__fec_chan(chan)]

    def __setitem__(self, chan, value):
        """Set FEC channel value"""
        ezca[self.__fec_chan(chan)] = value

    ##########
    # STATE_WORD

    @property
    def STATE_WORD(self):
        return int(self['STATE_WORD'])

    @property
    def excitation_active(self):
        word = self.STATE_WORD
        return bool(word&(1<<8))

    ##########
    # SDF

    @property
    def SDF_DIFF_CNT(self):
        return int(self['SDF_DIFF_CNT'])

    def load_snap(self, snap):
        """Load the specified snap file.

        """
        log("SDF LOAD: %s: '%s'" % (self.name, snap))
        self['SDF_NAME'] = snap
        time.sleep(0.1)
        self['SDF_RELOAD'] = 1

    def sdf_get_request(self):
        """Get requested SDF snap file."""
        return self['SDF_NAME']

    def sdf_get_loaded_table(self):
        """Get SDF snap currently loaded to TABLE."""
        return self['SDF_LOADED']

    def sdf_get_loaded_edb(self):
        """Get SDF snap currently loaded to EPICS db."""
        return self['SDF_LOADED_EDB']

    def sdf_load(self, snap, loadto='both', wait=True):
        """Load SDF snap file to table or EPICS db.

        The `loadto` option can be used to specify how the snap file
        is loaded.  The options are:

          'both': load into both SDF table and EPICS db (default)
         'table': load into SDF table only
           'edb': load into EPICS db only

        """
        lopts = {
            'both':  1,
            'table': 2,
            'edb':   4,
            }
        assert loadto in lopts.keys(), "Invalid `loadto` value (valid opts: %s)." % lopts.keys()

        s = loadto
        if loadto == 'both':
            s = 'table+edb'
        log("SDF LOAD: %s %s: '%s'" % (self.name.upper(), s, snap))

        self['SDF_NAME'] = snap
        self['SDF_RELOAD'] = lopts[loadto]

        # wait for the readbacks to confirm that the file has been loaded
        if loadto in ['edb', 'both'] and wait:
            while self.sdf_get_loaded_edb() != snap:
                time.sleep(0.01)
        if loadto in ['table', 'both'] and wait:
            while self.sdf_get_loaded_table() != snap:
                time.sleep(0.01)


##########
# Beckhoff SDF model tools

def get_name_from_pseudo_dcuid(dcuid):
    """Since the autoBurt.req's are created the same time as the model,
    look through the models in the target area with names that end in
    sdf to find a DCUID.
    """
    # This also covers the case of no name and no dcuid initialized
    if dcuid < 1024:
        raise ValueError('pseudo-dcuids must be greater than 1023')
    # Make a list of all of the possible pseudo-FEs by looking through
    # the target dir and then filtering out the ones that end in sdf
    target_dir_files = [filenames for filenames in os.listdir('{}/target/'.format(RTCDS))]
    pseudo_mod_poss = [fm for fm in target_dir_files if re.match('{}.*sdf'.format(IFO.lower()), str(fm))]
    # Look into each possibilities autoburt.req and see if it has a DCUID
    # that matches the input
    for model in pseudo_mod_poss:
        with open('{rtcds}/target/{model}/{model}epics/autoBurt.req'.format(rtcds=RTCDS, model=model)) as f:
            pattern = '{IFO}:FEC-(....)_CPU_METER.HIGH'.format(IFO=IFO)
            for chan in f:
                if re.match(pattern, chan):
                    dcuid_infile = int(re.search('-([0-9]*)_', chan).group(1))
                    if dcuid_infile == dcuid:
                        return re.match('{}(.*)sdf'.format(IFO.lower()), model).group(1)
                    else:
                        break
    # If it reached this point then there isnt a model matching the dcuid
    raise ValueError('dcuid: {}, does not match a model in the target'.format(dcuid))


def get_pseudo_dcuid_from_name(fullmodname):
    """Look through the target area for the full model name
    then through its autoBurt.req to find the DCUID in a
    FEC channel.
    """
    modname_full = '{}sdf'.format(fullmodname)
    with open('{rtcds}/target/{model}/{model}epics/autoBurt.req'.format(rtcds=RTCDS, model=modname_full)) as f:
        pattern = '{IFO}:FEC-(....)_CPU_METER.HIGH'.format(IFO=IFO)
        for chan in f:
            if re.match(pattern, chan):
                return int(re.search('-([0-9]*)_', chan).group(1))


def get_pseudo_frontends():
    """Search through all of the models autoBurt.req in the
    target area for a DCUID that is four digits.
    """
    # Make a list of all of the possible pseudo-FEs by looking through
    # the target dir and then filtering out the ones that end in sdf
    target_dir_files = [filenames for filenames in os.listdir('{}/target/'.format(RTCDS))]
    pseudo_mod_poss = [fm for fm in target_dir_files if re.match('{}.*sdf'.format(IFO.lower()), str(fm))]
    # Look into each possibilities autoburt.req for a 4 digit dcuid
    dcuid_re = re.compile('{IFO}:FEC-(....)_CPU_METER.HIGH'.format(IFO=IFO))
    model_re = re.compile('{}(.*)sdf'.format(IFO.lower()))
    for model in pseudo_mod_poss:
        with open('{rtcds}/target/{model}/{model}epics/autoBurt.req'.format(rtcds=RTCDS, model=model)) as f:
            for chan in f:
                dcuid = dcuid_re.match(chan)
                if dcuid:
                    yield (model_re.match(model).group(1), dcuid.group(1))


#########
# Beckoff SDF class interface

class CDSBeckhoffSDF(object):
    """Interface for the, mostly, Beckhoff SDF models"""
    def __init__(self, name=None, dcuid=None):
        """Initalize with the front end model `name` or dcuid.
        Make sure to specify which one

        Name should be without leading <ifo> prefix:

        e.g. 'sysecatx1plc2', 'hpipumpctrl', etc.

        If `dcuid` or name is not specified, it will be found.

        Usage example:
        >>>hepi_pump = CDSBeckhoffSDF(name='hpipumpctrl')
        >>>hepi_pump.DCUID
        1033
        >>>hepi_pump.SDF_DIFF_CNT
        2
        """
        self.__ifo = IFO.lower()
        self.__name = None
        if name:
            self.__name = name.lower()
        self.__DCUID = None
        if dcuid:
            self.__DCUID = int(dcuid)

    @property
    def name(self):
        """Model abbreviated name."""
        if not self.__name:
            self.__name = get_name_from_pseudo_dcuid(self.__DCUID)
        return self.__name

    @property
    def fullname(self):
        """Model name."""
        return '%s%s' % (self.__ifo, self.__name)

    @property
    def DCUID(self):
        """Model DCUID."""
        if not self.__DCUID:
            self.__DCUID = int(get_pseudo_dcuid_from_name(self.fullname))
        return self.__DCUID
    FEC = DCUID
    fec = DCUID
    dcuid = DCUID

    def __str__(self):
        return "<%s '%s' (dcuid: %d)>" % (self.__class__.__name__,
                                          self.name,
                                          self.dcuid,
                                          )

    def __fec_chan(self, chan):
        return 'FEC-%d_%s' % (self.dcuid, chan)

    def __getitem__(self, chan):
        """Get FEC channel value."""
        return ezca[self.__fec_chan(chan)]

    def __setitem__(self, chan, value):
        """Set FEC channel value"""
        ezca[self.__fec_chan(chan)] = value

    ##########
    # SDF

    @property
    def SDF_DIFF_CNT(self):
        return int(self['SDF_DIFF_CNT'])

    def load_snap(self, snap):
        """Load the specified snap file.

        """
        log("SDF LOAD: %s: '%s'" % (self.name, snap))
        self['SDF_NAME'] = snap
        time.sleep(0.1)
        self['SDF_RELOAD'] = 1

##################################################
# Command line

if __name__ == '__main__':
    import argparse
    import sys
    import os
    from ezca import Ezca
    ezca = Ezca()

    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--printmodels', action='store_true', help='Print the total list of the all models and their dcuids.')
    parser.add_argument('model', nargs='?', help='Give the model name or dcuid # and it will return the compliment.')
    args = parser.parse_args()

    ifo = os.getenv('ifo')
    lib = get_all_models()
    models = []
    for mod in lib:
        models.append((mod.DCUID, mod.name))
    models.sort()

    if args.printmodels:
        for mod in models:
            print('{}: {}'.format(mod[0], mod[1]))
    if args.model:
        try:
            mod = int(args.model)
        except ValueError:
            mod = args.model
        if type(mod) == int:
            for m in models:
                if m[0] == mod:
                    # Make sure to add the ifo prefix back in
                    print(ifo+m[1])
                    sys.exit()
        else:
            # Since cdslib does not the ifo prefixes here, pull it out if there
            if mod[:2] == ifo:
                mod = mod[2:]
            for m in models:
                if mod == m[1]:
                    print(m[0])
                    sys.exit()
            # Last stop error. Smarter ways to do this, but time
            raise ValueError('Model does not match our records!')
